FROM swift:5.6-focal AS builder

WORKDIR /app

RUN apt-get update && \
    apt-get install -y --no-install-recommends protobuf-compiler build-essential git && \
    git clone https://github.com/grpc/grpc-swift.git && \
    cd grpc-swift && \
    git checkout 1.8.2 && \
    make plugins

FROM ubuntu:20.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends protobuf-compiler libprotobuf-dev && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/lib/swift /usr/lib/swift
COPY --from=builder /app/grpc-swift/protoc-gen-swift /usr/local/bin/protoc-gen-swift
COPY --from=builder /app/grpc-swift/protoc-gen-grpc-swift /usr/local/bin/protoc-gen-grpc-swift
